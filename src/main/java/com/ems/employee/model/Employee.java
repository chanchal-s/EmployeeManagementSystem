package com.ems.employee.model;

public class Employee {

	private int employeeId;

	private String employeeName;

	private int employeeAge;

	private String departmentName;

	@Override

	public int hashCode() {

		final int prime = 31;

		int result = 1;

		result = prime * result + ((departmentName == null) ? 0 : departmentName.hashCode());

		result = prime * result + employeeAge;

		result = prime * result + employeeId;

		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());

		return result;

	}

	@Override

	public boolean equals(Object obj) {

		if (this == obj)

			return true;

		if (obj == null)

			return false;

		if (getClass() != obj.getClass())

			return false;

		Employee other = (Employee) obj;

		if (departmentName == null) {

			if (other.departmentName != null)

				return false;

		} else if (!departmentName.equals(other.departmentName))

			return false;

		if (employeeAge != other.employeeAge)

			return false;

		if (employeeId != other.employeeId)

			return false;

		if (employeeName == null) {

			if (other.employeeName != null)

				return false;

		} else if (!employeeName.equals(other.employeeName))

			return false;

		return true;

	}

	@Override

	public String toString() {

		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", employeeAge=" + employeeAge

				+ ", departmentName=" + departmentName + "]";

	}

	public Employee() {

		super();

	}

	public Employee(int employeeId, String employeeName, int employeeAge, String departmentName) {

		super();

		this.employeeId = employeeId;

		this.employeeName = employeeName;

		this.employeeAge = employeeAge;

		this.departmentName = departmentName;

	}

	public int getEmployeeId() {

		return employeeId;

	}

	public void setEmployeeId(int employeeId) {

		this.employeeId = employeeId;

	}

	public String getEmployeeName() {

		return employeeName;

	}

	public void setEmployeeName(String employeeName) {

		this.employeeName = employeeName;

	}

	public int getEmployeeAge() {

		return employeeAge;

	}

	public void setEmployeeAge(int employeeAge) {

		this.employeeAge = employeeAge;

	}

	public String getDepartmentName() {

		return departmentName;

	}

	public void setDepartmentName(String departmentName) {

		this.departmentName = departmentName;

	}

}
