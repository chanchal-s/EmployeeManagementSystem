package com.ems.employee.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ems.employee.model.Employee;
import com.ems.employee.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	EmployeeRepository employeeRepository;
	boolean createEmployee = false;
	boolean updateEmployee = false;
	boolean deleteEmployee = false;
	 List<Employee> employeeList = new ArrayList<Employee>();

	@Override
	public boolean createEmployee(Employee empObject) {
		createEmployee = employeeRepository.createEmpDetails(empObject);
		return createEmployee;
	}

	@Override
	public boolean updateEmployee(Employee empObject) {
		updateEmployee = employeeRepository.updateEmployeeDetails(empObject);
		return updateEmployee;
	}

	@Override
	public boolean deleteEmployee(Employee empObject) {
		deleteEmployee = employeeRepository.deleteEmployeeDetails(empObject);
		return deleteEmployee;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getAllEmployee(int empId) {
		employeeList = employeeRepository.getEmpDetails(empId);
		return employeeList;
	}

	

}
