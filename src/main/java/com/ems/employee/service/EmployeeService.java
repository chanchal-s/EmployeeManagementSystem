package com.ems.employee.service;

import java.util.List;

import com.ems.employee.model.Employee;

public interface EmployeeService {

	public List<Employee> getAllEmployee(int empId);
	public boolean createEmployee(Employee empObject);
	public boolean updateEmployee(Employee empObject);
	public boolean deleteEmployee(Employee empObject);
	
}
