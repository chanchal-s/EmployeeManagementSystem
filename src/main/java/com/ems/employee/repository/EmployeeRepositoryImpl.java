package com.ems.employee.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.ems.employee.model.Employee;
import com.ems.employee.util.DBConnection;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

	static Logger log = Logger.getLogger(EmployeeRepositoryImpl.class.getName());
	ResultSet rs = null;
	PreparedStatement pstm = null;
	Connection con = null;
	String selectSql = "Select *from ems.employee where employee.employee_id = ?";
	String inserSql = "Insert into ems.employee values(?,?,?,?)";
	String updateSql = "Update ems.employee set employee.employee_name = ?, employee.employee_age = ?, employee.employee_department = ? where employee.employee_id = ?";
	String deleteSql = "Delete from ems.employee where employee.employee_id = ?";
	boolean createFlag = false;
	boolean updateFlag = false;
	boolean deleteFlag = false;
	int record = 0;
	@SuppressWarnings("rawtypes")
	List employeeList = new ArrayList();
	 
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getEmpDetails(int empId) {
		log.info("calling menthos getEmpDetails!!");
		employeeList.clear();
		con = DBConnection.getDbConnection();
		try {
			pstm = con.prepareStatement(selectSql);
			pstm.setInt(1, empId);
			rs = pstm.executeQuery();
			while (rs.next()) {
				employeeList.add(rs.getInt(1));
				employeeList.add(rs.getString(2));
				employeeList.add(rs.getInt(3));
				employeeList.add(rs.getString(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				con.close();
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return employeeList;
	}

	@Override
	public boolean createEmpDetails(Employee empObject) {
		try {
			log.info("calling menthos createEmpDetails!! "+empObject.getEmployeeId());
			employeeList.clear();
			employeeList = getEmpDetails(empObject.getEmployeeId());
			if(employeeList.size() == 0) {
				con = DBConnection.getDbConnection();
				pstm = con.prepareStatement(inserSql);
				pstm.setInt(1, empObject.getEmployeeId());
				pstm.setString(2, empObject.getEmployeeName());
				pstm.setInt(3, empObject.getEmployeeAge());
				pstm.setString(4, empObject.getDepartmentName());			
				record = pstm.executeUpdate();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (record > 0) {
			createFlag = true;
		}
		return createFlag;
	}

	@Override
	public boolean updateEmployeeDetails(Employee empObject) {
		try {
			log.info("calling menthos updateEmpDetails!! "+empObject.getEmployeeId());
			employeeList.clear();
			employeeList = getEmpDetails(empObject.getEmployeeId());
			if(employeeList.size() > 0) {
				con = DBConnection.getDbConnection();
				pstm = con.prepareStatement(updateSql);
				pstm.setInt(4, empObject.getEmployeeId());
				pstm.setString(1, empObject.getEmployeeName());
				pstm.setInt(2, empObject.getEmployeeAge());
				pstm.setString(3, empObject.getDepartmentName());			
				record = pstm.executeUpdate();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (record > 0) {
			updateFlag = true;
		}
		return updateFlag;
	}

	@Override
	public boolean deleteEmployeeDetails(Employee empObject) {
		try {
			log.info("calling menthos deleteEmpDetails!!  "+empObject.getEmployeeId());
			employeeList.clear();
			employeeList = getEmpDetails(empObject.getEmployeeId());
			if(employeeList.size() > 0) {
				con = DBConnection.getDbConnection();
				pstm = con.prepareStatement(deleteSql);
				pstm.setInt(1, empObject.getEmployeeId());		
				record = pstm.executeUpdate();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (record > 0) {
			deleteFlag = true;
		}
		return deleteFlag;
	}

}
