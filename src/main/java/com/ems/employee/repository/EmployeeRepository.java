package com.ems.employee.repository;

import java.util.List;

import com.ems.employee.model.Employee;

public interface EmployeeRepository {
	
	@SuppressWarnings("rawtypes")
	public List getEmpDetails(int empId);
	public boolean createEmpDetails(Employee empObject);
	public boolean updateEmployeeDetails(Employee empObject);
	public boolean deleteEmployeeDetails(Employee empObject);

}
