package com.ems.employee.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ems.employee.model.Employee;
import com.ems.employee.service.EmployeeService;

@RestController
@RequestMapping(value = "/ems")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	boolean createEmployee = false;
    String message = null;
    List<Employee> employeeList = new ArrayList<Employee>();
    
	@RequestMapping(value = "/getEmployee/{employeeId}", method = RequestMethod.GET)
	public List<Employee> getEmployeeDetails(@PathVariable("employeeId") int empId) {

		employeeList = employeeService.getAllEmployee(empId);
		return employeeList;
	}

	@RequestMapping(value = "/createEmployee", method = RequestMethod.POST)
	public String createEmployeeDetails(@RequestBody Employee employee) {
		createEmployee= employeeService.createEmployee(employee);
		if(createEmployee) {
			message = "Employee created successfully!!";
		}else {
			message = "DB Error::: Employee"+employee.getEmployeeName()+" already exist.";
		}
		return message;		
	}
	
	@RequestMapping(value = "/updateEmployee", method = RequestMethod.PUT)
	public String updateEmployeeDetails(@RequestBody Employee employee) {
		createEmployee= employeeService.updateEmployee(employee);
		if(createEmployee) {
			message = "Employee updated successfully!!";
		}else {
			message = "DB Error::: Employee "+employee.getEmployeeName()+ " details doen not found";
		}
		return message;		
	}
	
	@RequestMapping(value = "/deleteEmployee", method = RequestMethod.DELETE)
	public String deleteEmployeeDetails(@RequestBody Employee employee) {
		createEmployee= employeeService.deleteEmployee(employee);
		if(createEmployee) {
			message = "Employee deleted successfully!!";
		}else {
			message = "DB Error::: Employee "+employee.getEmployeeName()+ " details doen not found";
		}
		return message;		
	}

}
